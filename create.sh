#!/bin/sh

echo 'Create new ISO'

if [ "$#" -eq 0 ]; then
	echo "No name specified"
	exit 0
fi

NAME="$1"
NAMEDIR=./out/"$NAME"

if [ -d "$NAMEDIR" ]; then
	echo 'removing old config'
	sudo rm -r "$NAMEDIR"
fi

echo 'create new base config'
mkdir -p "$NAMEDIR"
cp -r /usr/share/archiso/configs/releng/ "$NAMEDIR"

echo 'apply config changes'
BASEDIR="$NAMEDIR"/releng
mkdir -p "$BASEDIR"/airootfs/etc/skel/.ssh
touch "$BASEDIR"/airootfs/etc/skel/.ssh/authorized_keys

while [ -n "$2" ]; do
	echo "copy key <$2>"
        cat "$2" >> "$BASEDIR"/airootfs/etc/skel/.ssh/authorized_keys
	cp "$2" "$BASEDIR"/airootfs/etc/skel/.ssh/
	shift
done

cp customize_airootfs.sh "$BASEDIR"/airootfs/root/

echo "git" >> "$BASEDIR"/packages.x86_64

echo 'start build'
sudo sh "$BASEDIR"/build.sh -v -w "$BASEDIR/work" -P "Rudis Infrastruktur" -L "$NAME"_"$(date +%Y%m)" -N "$NAME"

